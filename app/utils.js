'use strict'

const fs = require('fs')
const { resolve, join } = require('path')
const mongoose = require('mongoose')

exports.responseHelper = (req, res, next) => {
  return {
    created: (data, isNext = true) => {
      res.status(201).json(data)
      if (isNext) {
        next()
      }
    },
    data: (data, isNext = true) => {
      res.status(200).json(data)
      if (isNext) {
        next()
      }
    },
    badRequest: (data = {}, isNext = true) => {
    //   logger.debug('bad request')
      res.status(400).json(data)
      if (isNext) {
        next()
      }
    },
    authorization: (data = {}, isNext = true) => {
    //   logger.debug('authorization')
      res.status(401).json(data)
      if (isNext) {
        next()
      }
    },
    notFound: (data = {}, isNext = true) => {
    //   logger.debug('no data found')
      res.status(404).json(data)
      if (isNext) {
        next()
      }
    },
    conflict: (data = {}, isNext = true) => {
    //   logger.debug('data conflict (existed)')
      res.status(409).json(data)
      if (isNext) {
        next()
      }
    },
    forbidden: (data = {}, isNext = true) => {
    //   logger.debug('forbidden')
      res.status(403).json(data)
      if (isNext) {
        next()
      }
    },
    error: (err) => {
    //   logger.warn(err)
      if (process.env.NODE_ENV !== 'production') {
        next(err)
      }
      else {
        next(new Error('We are Hiring !! Please, Contact me :'))
      }
    },
    info: (err, isNext = true) => {
    //   logger.warn(err)
      if (isNext) {
        next()
      }
    },
  }
}

