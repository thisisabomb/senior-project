const mongoose = require('mongoose')

let AttractionSchema = mongoose.Schema(
    {
        slug: { type: String, unique: true },
        name: { type: String },
        detailName: { type: String },
        phone: { type: String },
        dayOpen: { type: String },
        timeOpen: { type: String },
        province: { type: String },
        travelType: { typetype: String },
        detail: { type: String },
        photo: { type: Array, default: [] }
    },
    {
        timestamps: {createdAt: 'date_created', updatedAt: 'last_updated' },
        collection: 'attractions'    
    }
)

mongoose.model('Attraction', AttractionSchema)