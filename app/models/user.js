const mongoose = require('mongoose')

let UserSchema = mongoose.Schema({
  facebook: {
    id: String,
    email: String,
    name: String
  },
  local: {
    email: {
      type: String,
      minlength: 8,
      match: /\S+@\S+\.\S+/,
      unique: true
    }, // xxx@yyy.zz
    password: {
      type: String
    }
  },
  // username: {
  //   type: String,
  //   unique: true,
  //   required: true,
  // },
  // password: {
  //   type: String,
  //   required: true,
  // },
  // passwordConf: {
  //   type: String,
  //   required: true,
  // },
  // email: {
  //   type: String,
  //   unique: true,
  //   required: true,
  //   minlength: 10,
  //   // match: /\S+@\S+\.\S+/
  // },
  // address: {
  //   type: String,
  // },
  // gender: {
  //   type: String,
  // },
  // birthday: {
  //   type: String,
  // },
  // phone: {
  //   type: String,
  //   required: true,
  // },
  // profileImage: {
  //   type: String,
  //   default: ''
  // },
  // isVerified: {
  //   type: Boolean,
  //   default: false   // TODO
  // }
})

mongoose.model('User', UserSchema)