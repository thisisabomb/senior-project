const jwt = require('jsonwebtoken')
const path = require('path')
const { resolve } = require('path')
const crypto = require('crypto')
const redis = require('redis')
const configJwt = require(path.resolve('config/auth')).jwt
const redisClient = redis.createClient(process.env.REDIS_URL || 'redis://127.0.0.1:6379')



exports.respondToken = (req, res) => {
  res.status(200).json({
    success: true,
    token: {
      access: req.accessToken,
      refresh: req.refreshToken,
      expire_in: configJwt.expire_in,
      type: configJwt.type
    }
  })
}

exports.generateConfirmToken = (key) => {
  return function (req, res, next) {
    const user = req.user
    // console.log('user: ',user)
    // console.log('key: ',key)
    req.confirmToken = jwt.sign( { id: user.id, scopes: ['confirm'] }, key, {})
    console.log(req.confirmToken)
    next()
  }
}


const generateRedisKey = (userId, device = 'web')  => {
  return configJwt.prefix_redis_key + userId + '-' + device
}

exports.generateToken = (key) => {
  return function (req, res, next) {
    let user = req.user
    console.log('gen token: ',user)
    let jti = crypto.randomBytes(configJwt.jti_bytes_len).toString('hex')
    req.refreshToken = generateRefreshToken(key, user, jti)
    let device = req.body.device || req.get('X-DEVICE') || 'web'
    let redisKey = generateRedisKey(user.id, device)
    redisClient.set(redisKey, jti)

    user.scopes = [user.type || 'user']
    req.accessToken = generateAccessToken(key, user)
    console.log('access token: ', req.accessToken)
    console.log(req)
    // console.log(user)
    next()
  }
}

const generateAccessToken = (key, user) => {
  // logger.info(`generate access token for ${user.id}`)
  return jwt.sign(
    {
      id: user.id,
      scopes: user.scopes
    },
    key,
    {
      expiresIn: configJwt.access_token_expire
    }
  )
}

const generateRefreshToken = (key, user, jti) => {
  // logger.info(`generate refresh token for ${user.id}`)
  return jwt.sign(
    {
      id: user.id,
      scopes: ['refresh']
    },
    key,
    {
      expiresIn: configJwt.refresh_token_expire,
      jwtid: jti
    }
  )
}
