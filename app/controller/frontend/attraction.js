const { resolve } = require('path')
const mongoose = require('mongoose')
const Attraction = mongoose.model('Attraction')
const { responseHelper } = require(resolve('app/utils'))
const merge = require('lodash/merge')

exports.get = async (req, res, next) => {
    const response = responseHelper(req, res, next)
    try {
        const { slug } = req.params
        const attr = await Attraction.findOne({ slug })
        // console.log(attr)
        response.data(attr)
    }
    catch(e) {
        response.error(e)
    }
}

exports.list = async (req, res, next) => {
    const response = responseHelper(req, res, next)
    try {
        console.log('in List')
        const filter = {
            skip: 0,
            limit: 10,
            order: '-date_created',
          }
        merge(filter, req.query.filter)
        const attrs = await Attraction
            .find()
            .sort(filter.order)
            .skip(+filter.skip)
            .limit(+filter.limit)
        // console.log('attr: ',attrs)
        response.data(attrs)
    }
    catch(e) {
        response.error(e)
    }
}
