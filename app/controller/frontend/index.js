const { resolve } = require('path')
const path = require('path')
const join = path.join
const express =  require('express')
const mongoose = require('mongoose')
const User = mongoose.model('User')
const jwt = require(resolve('app/jwt'))
const python = require(resolve('app/controller/frontend/python'))
const attraction = require(resolve('app/controller/frontend/attraction'))
const expressJwt = require('express-jwt')
const { PythonShell } = require('python-shell')

module.exports = (app, passport) => {
  const authenOptions = { session: false }
  const secretKey = app.get('superSecret')
  const authenticate = expressJwt({
    secret: secretKey
  })
  let router = express.Router()

  const respondSignup = (req, res, next) => {
    res.status(200).json({
      success: true,
      message: 'sent confirmed mail'
    })
    next()
  }

  // Authentication //
  router.post('/signup', passport.authenticate('local-signup', authenOptions), jwt.generateConfirmToken(secretKey), respondSignup)

  router.post('/signin',passport.authenticate('local-login', authenOptions), jwt.generateToken(secretKey), jwt.respondToken)

  router.post('/facebook', passport.authenticate('facebook', {session: false}), jwt.generateToken(secretKey), jwt.respondToken)

  router.get('/test',authenticate, function (req, res, next){
    console.log(req)
  })

  // User //
  

  // Python //
  router.get('/python',
    python.scrapData)

  router.get('/attr',
    attraction.list
  )

  // Attraction //
  router.get('/attr/:slug',
    attraction.get 
  )


  return router
}