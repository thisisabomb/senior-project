const fs = require('fs')
const path = require('path')
const mongoose = require('mongoose')
const User = mongoose.model('User')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const FB = require('fb')

const generateHash = (password) => {
  let salt = bcrypt.genSaltSync(10);       // TODO random salt
  return bcrypt.hashSync(password, salt)
}

const validPass = (password, user) => {
  console.log(user)
  return bcrypt.compareSync(password, user.local.password)
}

const facebookLogin = async (req, token, done) => {
  FB.api('me', {
    fields: ['id', 'email', 'name', 'picture.width(320){url}'],
    access_token: token,
  }, async profile => {
    console.log(profile)
    return done(null, false)
    // logger.info(`facebook login as ${profile.name}}`)
    // if (profile && profile.error) {
    //   done(profile.error)
    // }
    // else {
    //   if (!req.user) { // check if the user is already logged in
    //     try {
    //       const user = await User.findOne({ 'facebook.id': profile.id })
    //       if (user) {
    //         return done(null, user)
    //       }
    //       else {
    //         const user = new User()
    //         await new Achievement({ user_id: user._id }).save()
    //         saveFacebookUser(user, profile, token, done)
    //       }
    //     }
    //     catch(err) {
    //       done(err)
    //     }
    //   }
    //   else { // existed user link with facebook
    //     try {
    //       const user = await User.findById(req.user.id)
    //       saveFacebookUser(user, profile, token, done)
    //     }
    //     catch(err) {
    //       done(err)
    //     }
    //   }
    // }
  })

}

function localSignup (req, email, password, done) {
  User.findOne({ 'local.email': email })
    .then(async function (existingUser) {
      if (existingUser) {
        var err = new Error()
        err.name = 'UnauthorizedError'
        err.message = 'user existed'
        err.status = 401
        return done(err) // user existed
      }
      else { // new user
        const hash = generateHash(password)
        console.log('email ', email)
        const user = await new User({
          'local.email': email,
          'local.password': hash
          // email: email,
          // password: hash,
          // passwordConf: hash
        }).save()
        done(null, user)
        // logger.info(`create new user ${email}`)

        // saveLocalUser(new User(), email, password, req.body.name, done)
      }
      console.log('in local sign up')
    })
    .catch(done)
}

function localLogin (req, email, password, done) {
  console.log('Sign in')
  User.findOne({ 'local.email': email })
    .then(function (user) {
      let err = null
      console.log('in Then', user)
      if (!user) {
        user = null
        err = new Error()
        err.name = 'UnauthorizedError'
        err.message = 'user not found or invalid user'
        err.status = 401
        // logger.info(`${email} - ${err.message}`)
      }
      else if (!validPass(password, user)) {
        user = null
        err = new Error()
        err.name = 'UnauthorizedError'
        err.message = 'user not found or invalid user'
        err.status = 401
      }
      done(err, user)
    })
    .catch(done)
}

module.exports = {
  // facebookLogin: facebookLogin,
  localLogin: localLogin,
  localSignup: localSignup,
  facebookLogin: facebookLogin
}
