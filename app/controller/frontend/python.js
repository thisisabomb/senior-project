const fs = require('fs')
const { resolve } = require('path')
const { responseHelper } = require(resolve('app/utils'))

exports.scrapData = (req, res, next) => {
    const response = responseHelper(req, res, next)
    try {
        const { body } = req
        const spawn = require('child_process').spawn;
        const pythonPath = resolve()
        // console.log(join(pythonPath,'/python/bus.js'))
        // console.log('in scrapData',body)
        
        const process = spawn('python', ['-u','/Users/khathawut/Documents/My works/CPE KU75/Year 4/senior-project/backend/python/train.py',
        body.source,
        body.destination,
        body.date
        // 'Chiang-Mai',
        // '05/04/2019',
        // '05/09/2019'
        ]);
        let dataString = ''
        process.stdout.on('data', function (data) {
            console.log('in')
            dataString += data.toString();
        });
        process.stdout.on('end', function(){
            // console.log('Sum of numbers=',dataString);
            res.json(JSON.parse(dataString));
        });
    }
    catch(e) {
        response.error(e)
    }
}