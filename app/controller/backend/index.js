const { resolve } = require('path')
const path = require('path')
const join = path.join
const express =  require('express')
const mongoose = require('mongoose')
const User = mongoose.model('User')
const jwt = require(resolve('app/jwt'))
const expressJwt = require('express-jwt')
const storeAttr = require(resolve('app/controller/backend/storeAttractions'))

module.exports = (app, passport) => {
  const authenOptions = { session: false }
  const secretKey = app.get('superSecret')
  const authenticate = expressJwt({
    secret: secretKey
  })
  let router = express.Router()

  router.get('/', (req, res, next) => res.json({ messagea: 'Welcome to Backend!!!' }))

  router.post('/store-data',
    storeAttr.initData
  )


  return router
}