const fs = require('fs')
const mongoose = require('mongoose')
const Attraction = mongoose.model('Attraction')
const uniqid = require('uniqid')


exports.initData = (req, res, next) => {
    // console.log(`${__dirname}/../../temp/attraction-json`)
    const path = `${__dirname}/../../../temp/attraction-json`
    const files = fs.readdirSync(path)
    files.forEach( async file => {
        const rawJson = fs.readFileSync(`${path}/${file}`)
        const attrs = await JSON.parse(rawJson)
        // console.log(attrs.attraction[0])  
        let count = 1
        attrs.attraction.forEach( async att => {
            let slug = uniqid()
            await Attraction({
                ...att,
                phone: att.tel,
                slug: slug
            }).save()
            .then(console.log('Success: ', count++))
        })
    })
} 