'use strict'

const fs = require('fs')
const path = require('path')
const join = path.join
const { resolve } = require('path')
const express = require('express')
let passport = require('passport')
let cookieParser = require('cookie-parser')
let session = require('express-session')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const models = join(__dirname, 'app/models/')
const app = express()
const port = 3000
const DB = 'mongodb://127.0.0.1:27017/Travel'
const FacebookStrategy = require('passport-facebook').Strategy


mongoose.set('useCreateIndex', true);
// passport.use(new FacebookStrategy({
//     clientID: '169977693671988',
//     clientSecret: '66eb0eb2c6c0cca97ff84df9c9c7e142',
//     callbackURL: "http://localhost:3000/auth/facebook/callback"
//   },
//   function(accessToken, refreshToken, profile, done) {
//     //ส่วนนี้จะเอาข้อมูลที่ได้จาก facebook ไปทำอะไรต่อก็ได้
//     done(null, profile) //เสร็จแล้วให้เรียกฟังก์ชั่นนี้
//   }
// ))

passport.serializeUser(function(user, done) {
  done(null, user) //อยากส่งอะไรไปเก็บใน session
})
passport.deserializeUser(function(obj, done) {
  done(null, obj) //เอาของที่เก็บใน session มาใช้ต่อ
})

mongoose.Promise = global.Promise
mongoose.connect(DB,{ useNewUrlParser: true }).then(
  () => {
    console.log('success')
  },
  err => {
    console.log(`Error: ${err}`)
  }
)

// Bootstrap models
fs.readdirSync(models)
  .filter(file => ~file.indexOf('.js'))
  .forEach(file => require(join(models, file)))

require('./config/passport')(passport)

app.set('superSecret', 'server secret555')

// Config body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(cookieParser());
app.use(session({ secret: 'keyboard cat', resave: true, saveUninitialized: true }))
app.use(passport.initialize())
app.use(passport.session())

module.exports = app


app.get('/', (req, res) => {
  res.send('please login')
})

const appRouter = require(resolve('app/controller/frontend'))
const APP_PATH =  '/'
app.use(APP_PATH, appRouter(app, passport))

const appRouterBackend = require(resolve('app/controller/backend'))
const APP_BACK_PATH = '/backend'
app.use(APP_BACK_PATH, appRouterBackend(app, passport))


app.listen(port,() => {
  console.log(`Port ${port}!`)
})
