import json
import regex
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import requests
from lxml import html
from collections import OrderedDict
import argparse
from bs4 import BeautifulSoup

def parse(url):
    for i in range(1):
        try:
            name = ''
            detailName = ''
            tel = ''
            dayOpen = ''
            timeOpen = ''
            province = ''
            travelType = ''
            detail = ''
            photo = []

            headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'}
            response = requests.get(url)
            parser = html.fromstring(response.text)
            soup = BeautifulSoup(response.text, 'html.parser')
            images = soup.find_all('img')
            for data in images:
                url = str(data['src'])
                if 'img_resize' in url:
                    photo.append(url)

            # print(photo)
            
            name_data_xpath = parser.xpath('/html/body/article/div[2]/div[2]//text()')
            detailname_data_xpath = parser.xpath('/html/body/article/div[3]/div[2]/div/div/div/div[2]/div[1]//text()')
            tel_data_xpath = parser.xpath('/html/body/article/div[3]/div[2]/div/div/div/div[2]/div[2]//text()')
            day_data_xpath = parser.xpath('/html/body/article/div[3]/div[2]/div/div/div/div[2]/div[3]//text()')
            time_data_xpath = parser.xpath('/html/body/article/div[3]/div[2]/div/div/div/div[2]/div[4]//text()')
            province_data_xpath = parser.xpath('/html/body/article/div[3]/div[3]/div/div[2]/div[1]//text()')
            type_data_xpath = parser.xpath('/html/body/article/div[3]/div[3]/div/div[2]/div[2]//text()')
            detail_data_xpath = parser.xpath('/html/body/article/div[3]/div[3]/div/div[3]//text()')


            for data in detail_data_xpath:
                for i in range(len(data)):
                    if (data[i] != '\r') and (data[i] != '\n') and (data[i] != '\t'):
                        detail = detail + data
                        break

            name = name_data_xpath[0].replace(' ','')

            if('ที่อยู่ : ' in detailname_data_xpath):
                detailName = detailname_data_xpath[1].replace('\xa0','')
            if(' เบอร์โทร : ' in tel_data_xpath):
                tel = tel_data_xpath[len(tel_data_xpath)-1]
            if('วันเปิดทำการ : ' in day_data_xpath):
                dayOpen = day_data_xpath[1]
            if('เวลาเปิดทำการ : ' in time_data_xpath):
                timeOpen = time_data_xpath[1]
            if('หมวดหมู่ : ' in type_data_xpath):
                travelType = type_data_xpath[1]

            if(province_data_xpath):
                province = province_data_xpath[1].replace(' ','')
            detail = detail.replace('\r\n\t','')

            travel_info = {
                'name': name,
                'detailName': detailName,
                'tel': tel,
                'dayOpen': dayOpen,
                'timeOpen': timeOpen,
                'province': province,
                'travelType': travelType,
                'detail': detail,
                'photo': photo
            }
            # print(travel_info)
            return travel_info


        except ValueError:
            print ("Rerying...")
            return {"error":"failed to process the page",}

if __name__=="__main__":
    with open('Attraction-thai-1.json') as data_file:
        data = json.load(data_file)

    scraped_data = []
    count = 1

    for url in data['attraction']:
        print('count: ',count)
        print ("travel :",url)
        scraped_data.append(parse(url))
        count += 1

    # parse('https://thai.tourismthailand.org/%E0%B8%AA%E0%B8%96%E0%B8%B2%E0%B8%99%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%97%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A7/%E0%B8%A0%E0%B8%B9%E0%B8%97%E0%B8%AD%E0%B8%81--6314')

    print ("Writing data to output file")
    with open('Travel-1-results.json','w') as fp:
        json.dump(scraped_data,fp,indent = 4,ensure_ascii=False)
