const LocalStrategy = require('passport-local').Strategy
const TokenStrategy = require('passport-accesstoken').Strategy

const { resolve } = require('path')
const auth = require(resolve('app/controller/frontend/auth'))

module.exports = (passport) => {
  passport.use('local-signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  }, auth.localSignup ))

  passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  }, auth.localLogin ))

  passport.use('facebook', new TokenStrategy({
    passReqToCallback: true
  }, auth.facebookLogin ))
}
